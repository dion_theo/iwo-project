#!/bin/bash

# Counting all the lines, since every line contains only one tweet
echo 'The number of tweets in the sample:'
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

# Filtering all the double tweets out and then counting the lines
echo 'The number of unique tweets:'
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l

# Again filtering all the double tweets out and searching for RT
# at the beginning of the line, as those lines are retweets. Finally I count the lines
echo 'Number of (unique) retweets:'
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep '^RT' | wc -l

# First searching for tweets that don't begin with RT, so no retweets.
# Then filtering all the double tweets out, without sorting them. Finally printing the first 20 tweets
echo 'The first 20 unique tweets in the sample that are not retweets:'
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | grep -v '^RT' | awk '!x[$0]++' | head -n20
