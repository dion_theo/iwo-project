#!/bin/bash

# Telt hoevaak een afkorting van een rechtse partij of een andere manier van schrijven daarvan voorkomt
vpvv=$(cat tweets.txt | python get-gender.py | grep "^female" | grep -iwe "PVV" -iwe "PARTIJ VOOR DE VRIJHEID" | wc -l)

vvvd=$(cat tweets.txt | python get-gender.py | grep "^female" | grep -iwe "VVD" -iwe "VOLKSPARTIJ VOOR VRIJHEID EN DEMOCRATIE" | wc -l)

vcda=$(cat tweets.txt | python get-gender.py | grep "^female" | grep -iwe "CDA" -iwe "CHRISTEN-DEMOCRATISCH APPÈl" | wc -l)

vsgp=$(cat tweets.txt | python get-gender.py | grep "^female" | grep -iwe "SGP" -iwe "STAATKUNDIG GEREFORMEERDE PARTIJ" | wc -l)

#Telt alle waardes op
rechtsv=$(($vpvv+$vvvd+$vcda+$vsgp))

echo "Aantal vrouwelijke, rechtse tweets:"
echo $rechtsv

# Telt hoevaak een afkorting van een linkse partij of een andere manier van schrijven daarvan voorkomt
vsp=$(cat tweets.txt | python get-gender.py | grep "^female" | grep "^female" | grep -iwe "SP" -iwe "SOCIALISTISCHE PARTIJ" | wc -l)

vgl=$(cat tweets.txt | python get-gender.py | grep "^female" | grep -iwe "GL" -iwe "GROENLINKS" | wc -l)

vpvdd=$(cat tweets.txt | python get-gender.py | grep "^female" | grep -iwe "PVDD" -iwe "PARTIJ VOOR DE DIEREN" | wc -l)

vpvda=$(cat tweets.txt | python get-gender.py | grep "^female" | grep -iwe "PVDA" -iwe "PARTIJ VAN DE ARBEID" | wc -l)

#Telt alle waardes op
linksv=$(($vsp+$vgl+$vpvdd+$vpvda))

echo "Aantal vrouwelijke, linkse tweets:"
echo $linksv

# Telt hoevaak een afkorting van een rechtse partij of een andere manier van schrijven daarvan voorkomt
mpvv=$(cat tweets.txt | python get-gender.py | grep "^male" | grep -iwe "PVV" -iwe "PARTIJ VOOR DE VRIJHEID" | wc -l)

mvvd=$(cat tweets.txt | python get-gender.py | grep "^male" | grep -iwe "VVD" -iwe "VOLKSPARTIJ VOOR VRIJHEID EN DEMOCRATIE" | wc -l)

mcda=$(cat tweets.txt | python get-gender.py | grep "^male" | grep -iwe "CDA" -iwe "CHRISTEN-DEMOCRATISCH APPÈl" | wc -l)

msgp=$(cat tweets.txt | python get-gender.py | grep "^male" | grep -iwe "SGP" -iwe "STAATKUNDIG GEREFORMEERDE PARTIJ" | wc -l)

#Telt alle waardes op
rechtsm=$(($mpvv+$mvvd+$mcda+$msgp))

echo "Aantal mannelijke, rechtse tweets:"
echo $rechtsm

# Telt hoevaak een afkorting van een linkse partij of een andere manier van schrijven daarvan voorkomt
msp=$(cat tweets.txt | python get-gender.py | grep "^male" | grep "^female" | grep -iwe "SP" -iwe "SOCIALISTISCHE PARTIJ" | wc -l)

mgl=$(cat tweets.txt | python get-gender.py | grep "^male" | grep -iwe "GL" -iwe "GROENLINKS" | wc -l)

mpvdd=$(cat tweets.txt | python get-gender.py | grep "^male" | grep -iwe "PVDD" -iwe "PARTIJ VOOR DE DIEREN" | wc -l)

mpvda=$(cat tweets.txt | python get-gender.py | grep "^male" | grep -iwe "PVDA" -iwe "PARTIJ VAN DE ARBEID" | wc -l)

#Telt alle waardes op
linksm=$(($msp+$mgl+$mpvdd+$mpvda))

echo "Aantal mannelijke, linkse tweets:"
echo $linksm
